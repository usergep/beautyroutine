// logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: []
  },
  onLoad() {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        var dd = util.formatTime(new Date(log));
        console.log(typeof dd);
        return util.formatTime(new Date(log))
      })
    })
  }
})
