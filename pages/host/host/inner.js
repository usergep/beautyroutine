// pages/host/host/inner.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  onGuideTap: function (event) {
    wx.openLocation({
      address: "福建省厦门市思明区民族路33号",
      errMsg: "chooseLocation:ok",
      latitude: 24.44579,
      longitude: 118.08243,
      name: "厦门市思明区人民政府",
      "success": (location) => {
        
      }
    });
  },
  call: function(){
    wx.makePhoneCall({
      phoneNumber: "15195986666",
      success: (result) => {
      },
      fail: () => {},
      complete: () => {}
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})