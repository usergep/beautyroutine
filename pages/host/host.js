import create from '../../utils/create';
import store from '../../store';
create(store, {
 
  /**
   * 页面的初始数据
   */
  data: {
    location: {
      name: "水帘洞",
    }
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },
  //导航
  onGuideTap: function (event) {
    wx.chooseLocation({
      "success": (location) => {
        this.setData({
          location
        })
      }
    });
  },
 intoStore: () => {
  wx.navigateTo({
    url: './host/inner?id=1',
    events: {
      // 为指定事件添加一个监听器，获取被打开页面传送到当前页面的数据
      acceptDataFromOpenedPage: function(data) {
        console.log(data)
      },
      someEvent: function(data) {
        console.log(data)
      }
    },
    success: function(res) {
      // 通过eventChannel向被打开页面传送数据
      // res.eventChannel.emit('acceptDataFromOpenerPage', { data: 'test' })
    }
  })
 }
 
 
})