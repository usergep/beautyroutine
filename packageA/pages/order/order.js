import create from '../../../utils/create';
import store from '../../../store';
create(store, {

  /**
   * 页面的初始数据
   */
  data: {
    moto:store.data.moto,
    proItem: [
      {
        img: "../../../cat.jpg",
        checked: false,
        text: "洗吹",
        price: "158",
        id: 1
      },{
        img: "../../../cat.jpg",
        checked: true,
        text: "吹",
        price: "1158",
        id: 2
      }
    ]
  },
  switch2Change: function(e) {
    const i = e.currentTarget.dataset.id;
    const proItem = this.data.proItem;
    proItem.map((item) => {
      if (item.id === i.id) {
        item.checked = !item.checked;
      }
      this.setData({
        proItem
      });
    });
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})