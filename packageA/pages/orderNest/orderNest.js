Page({

  data: {
    index:2,
    array: ['美国', '中国', '巴西', '日本'],
    orderDateId: "3",
    orderDateAll: [
      {
        oName: "今天",
        date: "3月2日",
        id: "1"
      },{
        oName: "明天",
        date: "3月3日",
        id: "2"
      },{
        oName: "后天",
        date: "3月4日",
        id: "3"
      },{
        oName: "周六",
        date: "3月5日",
        id: "4"
      },{
        oName: "周日",
        date: "3月6日",
        id: "5"
      },{
        oName: "周一",
        date: "3月7日",
        id: "6"
      },
    ],
    checkTimeId: "9:00-10:00",
    orderTimeAll: [
      {
        time: "9:00-10:00"
      },{
        time: "10:00-11:00"
      },{
        time: "11:00-12:00"
      },{
        time: "12:00-13:00"
      },{
        time: "13:00-14:00"
      },{
        time: "14:00-15:00"
      },{
        time: "15:00-16:00"
      },{
        time: "16:00-17:00"
      },{
        time: "17:00-18:00"
      },{
        time: "18:00-19:00"
      },{
        time: "19:00-20:00"
      },{
        time: "20:00-21:00"
      },{
        time: "21:00-22:00"
      },{
        time: "22:00-23:00"
      }
    ]
  },
  changeDate: function(e) {
    const orderDateAll = this.data.orderDateAll;
    orderDateAll.map((item) => {
      if (item.id === e.currentTarget.dataset.id.id) {
        this.setData({
          orderDateId: item.id
        });
      }
    });
  },
  changeTime: function(e) {
    this.setData({
      checkTimeId: e.currentTarget.dataset.id.time
    });
  },
  bindPickerChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
})